/*
 * @Author: chenl
 * @Date: 2023-12-01 10:44:43
 * @LastEditors: chenl
 * @LastEditTime: 2023-12-04 10:52:47
 * @Description: 提示
 */

module.exports = {
  donation: true, // 判断是否是需要donationConsole
  donationConsole() {
    const chalk = require('chalk');
    console.log(chalk.green(`> 欢迎使用土拉宝运营平台`));

    console.log(chalk.green(`> 当前配置地址` + process.env.VUE_APP_API));

    console.log(chalk.green(`> 如果您不希望显示以上信息，可在config中配置关闭`));
    console.log('\n');
  },
};
