// 页面搜索条件配置

import { validPhone } from '@/utils/validate';

export const searchColumns = (that) => [
  {
    prop: 'companyName',
    type: 'input',
    label: '企业名称',
    componentProp: {
      clearable: true,
      placeholder: '请输入创企业名称',
    },
    componentEvent: {},
  },
  {
    prop: 'companyCode',
    type: 'input',
    label: '企业编号',
    componentProp: {
      clearable: true,
      placeholder: '请输入企业编号',
    },
    componentEvent: {},
  },
  {
    prop: 'operateId',
    type: 'custom',
    label: '归属运营商',
  },
  {
    prop: 'cityLabel',
    type: 'input',
    label: '所在城市',
    componentProp: {
      clearable: true,
      placeholder: '请输入所在城市',
    },
    componentEvent: {},
  },
  {
    prop: 'adminNickName',
    type: 'input',
    label: '超管昵称',
    componentProp: {
      clearable: true,
      placeholder: '请输入超管昵称',
    },
    componentEvent: {},
  },
  {
    prop: 'adminPhone',
    type: 'input',
    label: '超管手机号',
    componentProp: {
      clearable: true,
      placeholder: '请输入超管手机号',
    },
    componentEvent: {},
  },
  {
    prop: 'authStatus',
    type: 'custom',
    label: '企业认证',
  },
  {
    prop: 'waterStatus',
    type: 'custom',
    label: '船运资质认证',
  },
  {
    prop: 'leader',
    type: 'input',
    label: '负责人姓名',
    componentProp: {
      clearable: true,
      placeholder: '请输入负责人姓名',
    },
    componentEvent: {},
  },
  {
    prop: 'leaderPhone',
    type: 'input',
    label: '负责人手机号',
    componentProp: {
      clearable: true,
      placeholder: '请输入负责人手机号',
    },
    componentEvent: {},
  },
  {
    prop: 'creatorName',
    type: 'input',
    label: '创建人姓名',
    componentProp: {
      clearable: true,
      placeholder: '请输入创建人姓名',
    },
    componentEvent: {},
  },
  {
    prop: 'creatorPhone',
    type: 'input',
    label: '创建人手机号',
    componentProp: {
      clearable: true,
      placeholder: '请输入创建人手机号',
    },
    componentEvent: {},
  },
  {
    prop: 'dateRange',
    type: 'date-range-picker',
    format: 'yyyy-MM-dd HH:mm:ss',
    label: '创建时间',
  },
  {
    prop: 'status',
    type: 'custom',
    label: '企业状态',
  },
];

// 页面表格配置
export const tableSchema = [
  {
    type: 'selection',
    fixed: 'left',
  },
  {
    label: '企业信息',
    dataIndex: 'companyNickName',
    width: '180',
    customSlot: 'companyNickName',
  },
  {
    label: '所在城市',
    dataIndex: 'cityLabel',
    width: '180',
  },
  {
    label: '归属运营商',
    dataIndex: 'deptName',
    width: '180',
  },
  {
    label: '超管信息',
    dataIndex: 'userName',
    width: '180',
    customSlot: 'userName',
  },
  {
    label: '企业认证',
    dataIndex: 'authStatusStr',
    width: '150',
    customSlot: 'authStatusStr',
  },
  {
    label: '船运资质认证',
    dataIndex: 'waterStatusStr',
    width: '150',
    customSlot: 'waterStatusStr',
  },
  {
    label: '负责人',
    dataIndex: 'leader',
    width: '180',
    customSlot: 'leader',
  },
  {
    label: '创建方式',
    dataIndex: 'signTypeStr',
    width: '100',
  },
  {
    label: '创建时间',
    dataIndex: 'createTime',
    width: '180',
  },
  {
    label: '状态',
    dataIndex: 'statusStr',
    width: '100',
    customSlot: 'statusStr',
  },
];

// 表格操作配置
export const tableActions = (handleUpdate, handleStatusChange, handleShowDetail) => [
  {
    width: 220,
    label: '操作',
    buttons: [
      {
        label: '查看详情',
        onClick: (row) => {
          handleShowDetail(row);
        },
      },
      {
        label: '编辑',
        onClick: (row) => {
          handleUpdate(row);
        },
      },
      {
        label: '禁用',
        onClick: (row) => {
          handleStatusChange(row);
        },
      },
      // {
      //   label: '调整归属企业',
      //   onClick: (row) => {
      //     handleSuperAdminTransfer(row);
      //   },
      // },
    ],
  },
];

const phoneValid = (rule, value, callback) => {
  if (!validPhone(value)) {
    callback(new Error('请输入正确的手机号'));
  } else {
    callback();
  }
};

// 新增/编辑企业弹窗表单配置
export const addOrEditFormConfig = [
  {
    prop: 'companyNickName',
    type: 'input',
    label: '企业名称',
    componentProp: {
      clearable: true,
      placeholder: '请输入企业名称',
    },
    componentEvent: {},
    rules: [{ required: true, trigger: 'blur', message: '企业名称不能为空' }],
  },
  {
    prop: 'citycopy',
    type: 'address',
    label: '所在城市',
    componentProp: {
      placeholder: '请选择所在城市',
    },
    componentEvent: {},
    rules: [{ required: true, trigger: 'blur', message: '所在城市不能为空' }],
  },
  {
    prop: 'operateId',
    type: 'select',
    label: '归属运营商',
    options: [],
    componentProp: {
      clearable: true,
      placeholder: '请选择归属运营商',
    },
    componentEvent: {},
    rules: [{ required: true, trigger: 'change', message: '请选运营商' }],
  },
  {
    prop: 'leader',
    type: 'input',
    label: '负责人',
    componentProp: {
      clearable: true,
      placeholder: '请输入负责人',
    },
    componentEvent: {},
    rules: [{ required: true, trigger: 'change', message: '请输入负责人名称' }],
  },
  {
    prop: 'leaderPhone',
    type: 'input',
    label: '负责人手机号',
    componentProp: {
      clearable: true,
      placeholder: '请输入负责人手机号',
      maxlength: 15,
    },
    componentEvent: {},
    rules: [
      { required: true, trigger: 'blur', message: '负责人电话不能为空' },
      { required: true, trigger: 'blur', validator: phoneValid },
    ],
  },
  {
    prop: 'companyAdminPhone',
    type: 'input',
    label: '超级管理员账号',
    componentProp: {
      clearable: true,
      placeholder: '请输入手机号',
      maxlength: 15,
    },
    componentEvent: {},
    rules: [
      { required: true, trigger: 'blur', message: '超管手机号不能为空' },
      { required: true, trigger: 'blur', validator: phoneValid },
    ],
  },
];
