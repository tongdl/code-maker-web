// 页面搜索条件配置
export const searchColumns = (that) => [];

// 页面表格配置
export const tableSchema = [
    {
      label: '用户信息',
      dataIndex: 'userNickName',
      customSlot: 'userNickName',
      width: '180',
    },
    {
      label: '用户ID',
      dataIndex: 'userId',
      width: '180',
    },
    {
      label: '身份标签',
      dataIndex: 'tag',
      width: '180',
    },
    {
      label: '实名认证',
      dataIndex: 'userAuthStatus',
      customSlot: 'userAuthStatus',
      width: '100',
    },
    {
      label: '真实姓名',
      dataIndex: 'name',
      width: '180',
    },
    {
      label: '身份证号',
      dataIndex: 'identityCard',
      width: '180',
    },
    {
      label: '船员资质认证',
      dataIndex: 'crewStatus',
      customSlot: 'crewStatus',
      width: '180',
    },
    {
      label: '团队角色',
      dataIndex: 'userTypeStr',
      width: '180',
    },
    {
      label: '加入时间',
      dataIndex: 'hireDate',
      width: '180',
    },
    {
      label: '账号状态',
      dataIndex: 'userStatus',
      customSlot: 'userStatus',
      width: '100',
    },
];

// 表格操作配置
export const tableActions = (handleRoleChange, handleQuit, handleShowDetail, transferLeader) => [
  {
    width: 300,
    label: '操作',
    buttons: [
      {
        label: '查看详情',
        onClick: (row) => {
          handleShowDetail(row);
        },
      },
      {
        label: '转移管理员',
        onClick: (row) => {
          transferLeader(row);
        },
      },
      {
        label: '更换角色',
        onClick: (row) => {
          handleRoleChange(row);
        },
      },
      {
        label: '移出企业',
        onClick: (row) => {
          handleQuit(row);
        },
      }
    ],
  },
];
