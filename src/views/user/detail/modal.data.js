// 页面表格配置
export const tableSchema = [
  {
    label: '企业信息',
    dataIndex: 'companyInfo',
    width: '200',
    customSlot: 'companyInfo',
  },
  {
    label: '所在城市',
    dataIndex: 'city',
    width: '90',
  },
  {
    label: '归属运营商',
    dataIndex: 'operateName',
    width: '180',
  },
  {
    label: '超管信息',
    dataIndex: 'adminInfo',
    width: '200',
    customSlot: 'adminInfo',
  },
  {
    label: '企业认证',
    dataIndex: 'companyAuthStatus',
    width: '100',
    customSlot: 'companyAuthStatusStr',
  },
  {
    label: '船运资质认证',
    dataIndex: 'waterStatus',
    width: '90',
    customSlot: 'waterStatusStr',
  },
  {
    label: '负责人',
    dataIndex: 'leader',
    width: '160',
    customSlot: 'leader',
  },
  {
    label: '团队角色',
    dataIndex: 'userTypeStr',
    width: '160',
  },
  {
    label: '加入企业时间',
    dataIndex: 'hireDate',
    width: '160',
  },
  {
    label: '状态',
    dataIndex: 'companyStatus',
    width: '100',
    customSlot: 'companyStatusStr',
  },
];

// 表格操作配置
export const tableActions = (handleCompanyDetail) => [
  {
    width: 220,
    label: '操作',
    buttons: [
      {
        label: '查看详情',
        onClick: (row) => {
          handleCompanyDetail(row.id);
        },
      },
    ],
  },
];
