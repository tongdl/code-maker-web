// 页面搜索条件配置
export const searchColumns = () => [
  {
    prop: 'nickName',
    type: 'input',
    label: '用户昵称',
    componentProp: {
      clearable: true,
      placeholder: '请输入用户昵称',
    },
    componentEvent: {},
  },
  {
    prop: 'mobile',
    type: 'input',
    label: '手机号',
    componentProp: {
      clearable: true,
      placeholder: '请输入手机号',
    },
    componentEvent: {},
  },
  {
    prop: 'gcbUserCode',
    type: 'input',
    label: '用户ID',
    componentProp: {
      clearable: true,
      placeholder: '请输入用户ID',
    },
    componentEvent: {},
  },
  {
    prop: 'sex',
    type: 'custom',
    label: '性别',
  },
  {
    prop: 'city',
    type: 'input',
    label: '所在城市',
    componentProp: {
      clearable: true,
      placeholder: '请输入所在城市',
    },
    componentEvent: {},
  },
  {
    prop: 'education',
    type: 'custom',
    label: '学历',
  },
  {
    prop: 'authStatus',
    type: 'custom',
    label: '实名认证',
  },
  {
    prop: 'name',
    type: 'input',
    label: '真实姓名',
    componentProp: {
      clearable: true,
      placeholder: '请输入真实姓名',
    },
    componentEvent: {},
  },
  {
    prop: 'identityCard',
    type: 'input',
    label: '身份证号',
    componentProp: {
      clearable: true,
      placeholder: '请输入身份证号',
    },
    componentEvent: {},
  },
  {
    prop: 'tag',
    type: 'custom',
    label: '身份标签',
  },
  {
    prop: 'crewAuthStatus',
    type: 'custom',
    label: '船员资质认证',
  },
  {
    prop: 'signType',
    type: 'custom',
    label: '注册方式',
  },
  {
    prop: 'date',
    type: 'date-picker',
    label: '注册时间',
    componentProp: {
      type: 'daterange',
      rangeSeparator: '至',
      startPlaceholder: '开始日期',
      endPlaceholder: '结束日期',
    },
    componentEvent: {},
  },
  {
    prop: 'status',
    type: 'custom',
    label: '账号状态',
  },
];

// 页面表格配置
export const tableSchema = [
  {
    type: 'selection',
    fixed: 'left',
  },
  {
    label: '用户信息',
    dataIndex: 'userInfo',
    width: '200',
    customSlot: 'userInfo',
  },
  {
    label: '用户ID',
    dataIndex: 'gcbUserCode',
    width: '120',
  },
  {
    label: '性别',
    dataIndex: 'sexStr',
    width: '50',
  },
  {
    label: '年龄',
    dataIndex: 'age',
    width: '50',
  },
  {
    label: '所在城市',
    dataIndex: 'city',
    width: '80',
  },
  {
    label: '学历',
    dataIndex: 'education',
    width: '80',
  },
  {
    label: '工作经验',
    dataIndex: 'experience',
    width: '70',
  },
  {
    label: '实名认证',
    dataIndex: 'authStatus',
    width: '70',
    customSlot: 'authStatusStr',
  },
  {
    label: '真实姓名',
    dataIndex: 'name',
    width: '70',
  },
  {
    label: '身份证号',
    dataIndex: 'identityCard',
    width: '150',
  },
  {
    label: '身份标签',
    dataIndex: 'tag',
    width: '100',
  },
  {
    label: '船员资质认证',
    dataIndex: 'crewAuthStatus',
    width: '100',
    customSlot: 'crewAuthStatusStr',
  },
  {
    label: '注册方式',
    dataIndex: 'signTypeStr',
    width: '80',
  },
  {
    label: '注册时间',
    dataIndex: 'createTime',
    width: '160',
  },
  {
    label: '账号状态',
    dataIndex: 'status',
    width: '80',
    customSlot: 'statusStr',
  },
];

export const tableActions = () => [
  {
    width: 120,
    label: '操作',
    customSlot: 'action',
  },
];
