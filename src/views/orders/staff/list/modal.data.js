// 页面搜索条件配置

export const searchColumns = () => [
  {
    prop: 'orderNo',
    type: 'input',
    label: '订单号',
    componentProp: {
      clearable: true,
      placeholder: '请输入订单号',
    },
    componentEvent: {},
  },
  {
    prop: 'title',
    type: 'input',
    label: '求职标题',
    componentProp: {
      clearable: true,
      placeholder: '请输入求职标题',
    },
    componentEvent: {},
  },
  {
    prop: 'nickName',
    type: 'input',
    label: '求职船员昵称',
    componentProp: {
      clearable: true,
      placeholder: '请输入求职船员昵称',
    },
    componentEvent: {},
  },
  {
    prop: 'mobile',
    type: 'input',
    label: '求职船员手机号',
    componentProp: {
      clearable: true,
      placeholder: '请输入求职船员手机号',
    },
    componentEvent: {},
  },
  {
    prop: 'name',
    type: 'input',
    label: '真实姓名',
    componentProp: {
      clearable: true,
      placeholder: '请输入真实姓名',
    },
    componentEvent: {},
  },
  {
    prop: 'age',
    type: 'input',
    label: '年龄低于',
    componentProp: {
      clearable: true,
      placeholder: '年龄低于',
    },
    componentEvent: {},
  },
  {
    prop: 'sex',
    type: 'custom',
    label: '性别',
  },
  {
    prop: 'city',
    type: 'input',
    label: '所在城市',
    componentProp: {
      clearable: true,
      placeholder: '请输入所在城市',
    },
    componentEvent: {},
  },
  {
    prop: 'education',
    type: 'custom',
    label: '学历',
  },
  {
    prop: 'experience',
    type: 'input',
    label: '工作经验超过',
    componentProp: {
      clearable: true,
      placeholder: '工作经验',
    },
    componentEvent: {},
  },
  {
    prop: 'credentialsType',
    type: 'custom',
    label: '证书资格类别',
  },
  {
    prop: 'credentialsLevel',
    type: 'custom',
    label: '证书等级',
  },
  {
    prop: 'deadline',
    type: 'date-picker',
    label: '截止日期',
    componentProp: {
      type: 'daterange',
      rangeSeparator: '至',
      startPlaceholder: '开始日期',
      endPlaceholder: '结束日期',
    },
    componentEvent: {},
  },
  {
    prop: 'createType',
    type: 'custom',
    label: '创建方式',
  },
  {
    prop: 'createTime',
    type: 'date-picker',
    label: '创建时间',
    componentProp: {
      type: 'daterange',
      rangeSeparator: '至',
      startPlaceholder: '开始日期',
      endPlaceholder: '结束日期',
    },
    componentEvent: {},
  },
];

// 页面表格配置
export const tableSchema = [
  {
    type: 'selection',
    fixed: 'left',
  },
  {
    label: '订单号',
    dataIndex: 'orderNo',
    width: '180',
  },
  {
    label: '求职标题',
    dataIndex: 'title',
    width: '180',
  },
  {
    label: '求职船员',
    dataIndex: 'crewInfo',
    width: '180',
    customSlot: 'crewInfo',
  },
  {
    label: '姓名',
    dataIndex: 'name',
    width: '120',
  },
  {
    label: '年龄',
    dataIndex: 'age',
    width: '100',
  },
  {
    label: '性别',
    dataIndex: 'sexStr',
    width: '180',
  },
  {
    label: '所在城市',
    dataIndex: 'city',
    width: '180',
  },
  {
    label: '学历',
    dataIndex: 'education',
    width: '90',
  },
  {
    label: '工作经验',
    dataIndex: 'experience',
    width: '160',
  },
  {
    label: '证书资格类别',
    dataIndex: 'credentialsTypeStr',
    width: '160',
  },
  {
    label: '证书等级',
    dataIndex: 'credentialsLevelStr',
    width: '160',
  },
  {
    label: '截止日期',
    dataIndex: 'endDate',
    width: '160',
  },
  {
    label: '创建方式',
    dataIndex: 'createTypeStr',
    width: '160',
  },

  {
    label: '创建人',
    dataIndex: 'createBy',
    width: '150',
  },
  {
    label: '创建时间',
    dataIndex: 'createTime',
    width: '150',
  },
  {
    label: '状态',
    dataIndex: 'stateStr',
    width: '100',
    customSlot: 'stateStr',
  },
];

// 表格操作配置
export const tableActions = () => [
  {
    width: 220,
    label: '操作',
    customSlot: 'action',
  },
];
