// 页面搜索条件配置

export const searchColumns = (that) => [
  {
    prop: 'businessesName',
    type: 'input',
    label: '商户名称',
    componentProp: {
      clearable: true,
      placeholder: '请输入商户名称',
    },
    componentEvent: {},
  },
  {
    prop: 'businessesCode',
    type: 'input',
    label: '商户编号',
    componentProp: {
      clearable: true,
      placeholder: '请输入商户编号',
    },
    componentEvent: {},
  },
  {
    prop: 'leader',
    type: 'input',
    label: '负责人',
    componentProp: {
      clearable: true,
      placeholder: '请输入负责人名称',
    },
    componentEvent: {},
  },
  {
    prop: 'leaderPhone',
    type: 'input',
    label: '负责人联系方式',
    componentProp: {
      clearable: true,
      placeholder: '请输入负责人联系方式',
    },
    componentEvent: {},
  },

];

// 页面表格配置
export const tableSchema = [
  {
    label: '商户名称',
    dataIndex: 'businessesName',
    width: '180',
  },
  {
    label: '商户编号',
    dataIndex: 'businessesCode',
    width: '180',
  },
  {
    label: '所在城市',
    dataIndex: 'cityCode',
    width: '120',
    customSlot: 'cityCode',
  },
  {
    label: '负责人姓名',
    dataIndex: 'leader',
    width: '100',
  },
  {
    label: '负责人联系方式',
    dataIndex: 'leaderPhone',
    width: '120',
  },
  {
    label: '备注',
    dataIndex: 'remark',
    width: '300',
  },
  {
    label: '关联',
    dataIndex: 'relatedWharf',
    width: '100',
    customSlot: 'relatedWharf',
  },
  {
    label: '创建人',
    dataIndex: 'createName',
    width: '100',
  },
  {
    label: '创建时间',
    dataIndex: 'createTime',
    width: '180',
  },
];

// 表格操作配置
export const tableActions = (updateBusinesses, deleteBusinesses) => [
  {
    width: 220,
    label: '操作',
    buttons: [
      {
        label: '编辑',
        onClick: (row) => {
          updateBusinesses(row);
        },
      },
      {
        label: '删除',
        onClick: (row) => {
          deleteBusinesses(row);
        },
      },
    ],
  },
];
