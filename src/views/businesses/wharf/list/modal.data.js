// 页面搜索条件配置

export const searchColumns = () => [
  {
    prop: 'wharfName',
    type: 'input',
    label: '码头名称',
    componentProp: {
      clearable: true,
      placeholder: '请输入码头名称',
    },
    componentEvent: {},
  },
  {
    prop: 'businessesId',
    type: 'custom',
    label: '归属商户',
  },
  {
    prop: 'portName',
    type: 'input',
    label: '所属港区',
    componentProp: {
      clearable: true,
      placeholder: '请输入所属港区',
    },
    componentEvent: {},
  },
  {
    prop: 'fullAddress',
    type: 'input',
    label: '码头地址',
    componentProp: {
      clearable: true,
      placeholder: '请输入码头地址',
    },
    componentEvent: {},
  },
  {
    prop: 'contacts',
    type: 'input',
    label: '联系人',
    componentProp: {
      clearable: true,
      placeholder: '请输入联系人',
    },
    componentEvent: {},
  },
  {
    prop: 'contactPhone',
    type: 'input',
    label: '联系方式',
    componentProp: {
      clearable: true,
      placeholder: '请输入联系方式',
    },
    componentEvent: {},
  },
  {
    prop: 'status',
    type: 'custom',
    label: '状态',
  },
];

// 页面表格配置
export const tableSchema = [
  {
    type: 'selection',
    fixed: 'left',
  },
  {
    label: '码头名称',
    dataIndex: 'wharfName',
    width: '180',
  },
  {
    label: '归属商户',
    dataIndex: 'businessesName',
    width: '180',
  },
  {
    label: '所属港区',
    dataIndex: 'portName',
    width: '200',
  },
  {
    label: '码头地址',
    dataIndex: 'fullAddress',
    width: '300',
  },
  {
    label: '联系人',
    dataIndex: 'contacts',
    width: '180',
  },
  {
    label: '创建时间',
    dataIndex: 'contactPhone',
    width: '150',
  },
  {
    label: '状态',
    dataIndex: 'statusStr',
    width: '100',
    customSlot: 'statusStr',
  },
];

// 表格操作配置
export const tableActions = (
  handleDetail,
  handleUpdate,
  handleUpdateBusinesses,
  handleDisable,
  handleDelete,
) => [
  {
    width: 280,
    label: '操作',
    buttons: [
      {
        label: '查看详情',
        onClick: (row) => {
          handleDetail(row);
        },
      },
      {
        label: '编辑',
        onClick: (row) => {
          handleUpdate(row);
        },
      },
      {
        label: '改归属商户',
        onClick: (row) => {
          handleUpdateBusinesses(row);
        },
      },
      {
        label: '禁用',
        onClick: (row) => {
          handleDisable(row);
        },
      },
      {
        label: '删除',
        onClick: (row) => {
          handleDelete(row);
        },
      },
    ],
  },
];
