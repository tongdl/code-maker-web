// 页面搜索条件配置

export const searchColumns = () => [
  {
    prop: 'shipName',
    type: 'input',
    label: '船舶名称',
    componentProp: {
      clearable: true,
      placeholder: '请输入船舶名称',
    },
    componentEvent: {},
  },
  {
    prop: 'aisCode',
    type: 'input',
    label: 'AIS编码',
    componentProp: {
      clearable: true,
      placeholder: '请输入AIS编码',
    },
    componentEvent: {},
  },
  {
    prop: 'shipType',
    type: 'custom',
    label: '船舶类型',
  },
  {
    prop: 'companyId',
    type: 'custom',
    label: '归属企业',
  },
  {
    prop: 'operatorId',
    type: 'custom',
    label: '归属运营商',
  },
  {
    prop: 'createType',
    type: 'custom',
    label: '创建方式',
  },
  {
    prop: 'createBy',
    type: 'input',
    label: '创建人姓名',
    componentProp: {
      clearable: true,
      placeholder: '请输入创建人姓名',
    },
    componentEvent: {},
  },
  {
    prop: 'createPhone',
    type: 'input',
    label: '创建人手机号',
    componentProp: {
      clearable: true,
      placeholder: '请输入创建人手机号',
    },
    componentEvent: {},
  },
  {
    prop: 'date',
    type: 'date-picker',
    label: '创建时间',
    componentProp: {
      type: 'daterange',
      rangeSeparator: '至',
      startPlaceholder: '开始日期',
      endPlaceholder: '结束日期',
    },
    componentEvent: {},
  },
  {
    prop: 'authStatus',
    type: 'custom',
    label: '平台认证状态',
  },
];

// 页面表格配置
export const tableSchema = [
  {
    type: 'selection',
    fixed: 'left',
  },
  {
    label: '船舶名称',
    dataIndex: 'shipName',
    width: '180',
  },
  {
    label: 'AIS编码',
    dataIndex: 'aisCode',
    width: '180',
  },
  {
    label: '船舶类型',
    dataIndex: 'shipTypeStr',
    width: '120',
  },
  {
    label: '载货吨位(吨)',
    dataIndex: 'shipTonLevel',
    width: '120',
  },
  {
    label: '建造日期',
    dataIndex: 'buildDate',
    width: '100',
  },
  {
    label: '归属企业',
    dataIndex: 'companyName',
    width: '180',
  },
  {
    label: '归属运营商',
    dataIndex: 'operatorName',
    width: '180',
  },
  {
    label: '创建方式',
    dataIndex: 'createTypeStr',
    width: '90',
  },
  {
    label: '创建人',
    dataIndex: 'createBy',
    width: '160',
  },
  {
    label: '创建时间',
    dataIndex: 'createTime',
    width: '150',
  },
  {
    label: '平台认证状态',
    dataIndex: 'authStatusStr',
    width: '100',
    customSlot: 'authStatusStr',
  },
];

// 表格操作配置
export const tableActions = (getShipDetail, updateShip, showUpdateCompanyDialog) => [
  {
    width: 220,
    label: '操作',
    buttons: [
      {
        label: '查看详情',
        onClick: (row) => {
          getShipDetail(row);
        },
      },
      {
        label: '编辑',
        onClick: (row) => {
          updateShip(row);
        },
      },
      {
        label: '调整归属企业',
        onClick: (row) => {
          showUpdateCompanyDialog(row);
        },
      },
    ],
  },
];
