// 页面搜索条件配置

export const searchColumns = () => [
  {
    prop: 'mobile',
    type: 'input',
    label: '船员手机号',
    componentProp: {
      clearable: true,
      placeholder: '请输入船员手机号',
    },
    componentEvent: {},
  },
  {
    prop: 'name',
    type: 'input',
    label: '船员姓名',
    componentProp: {
      clearable: true,
      placeholder: '请输入船员姓名',
    },
    componentEvent: {},
  },
  {
    prop: 'idCard',
    type: 'input',
    label: '船员身份证号码',
    componentProp: {
      clearable: true,
      placeholder: '请输入身份证号码',
    },
    componentEvent: {},
  },
  {
    prop: 'credentialsType',
    type: 'custom',
    label: '职务资格类别',
  },
  {
    prop: 'credentialsLevel',
    type: 'custom',
    label: '证书等级',
  },
  {
    prop: 'createType',
    type: 'custom',
    label: '创建方式',
  },
  {
    prop: 'createBy',
    type: 'input',
    label: '创建人姓名',
    componentProp: {
      clearable: true,
      placeholder: '请输入创建人姓名',
    },
    componentEvent: {},
  },
  {
    prop: 'createPhone',
    type: 'input',
    label: '创建人手机号',
    componentProp: {
      clearable: true,
      placeholder: '请输入创建人手机号',
    },
    componentEvent: {},
  },
  {
    prop: 'date',
    type: 'date-picker',
    label: '创建时间',
    componentProp: {
      type: 'daterange',
      rangeSeparator: '至',
      startPlaceholder: '开始日期',
      endPlaceholder: '结束日期',
    },
    componentEvent: {},
  },
  {
    prop: 'authStatus',
    type: 'custom',
    label: '平台认证状态',
  },
];

// 页面表格配置
export const tableSchema = [
  {
    label: '船员手机号',
    dataIndex: 'mobile',
    width: '120',
  },
  {
    label: '船员姓名',
    dataIndex: 'name',
    width: '90',
  },
  {
    label: '船员身份证号码',
    dataIndex: 'idCard',
    width: '180',
  },
  {
    label: '身份证有效期',
    dataIndex: 'idCardExpire',
    width: '200',
    customSlot: 'idCardExpire',
  },
  {
    label: '职务资格类别',
    dataIndex: 'credentialsTypeStr',
    width: '100',
  },
  {
    label: '证书等级',
    dataIndex: 'credentialsLevelStr',
    width: '90',
  },
  {
    label: '创建方式',
    dataIndex: 'createTypeStr',
    width: '90',
  },
  {
    label: '创建人',
    dataIndex: 'createBy',
    width: '160',
  },
  {
    label: '创建时间',
    dataIndex: 'createTime',
    width: '160',
  },
  {
    label: '平台认证状态',
    dataIndex: 'authStatusStr',
    width: '100',
    customSlot: 'authStatusStr',
  },
];

// 表格操作配置
export const tableActions = (handleDetail, handleUpdate) => [
  {
    width: 220,
    label: '操作',
    buttons: [
      {
        label: '查看详情',
        onClick: (row) => {
          handleDetail(row.id);
        },
      },
      {
        label: '编辑',
        onClick: (row) => {
          handleUpdate(row.id);
        },
      },
    ],
  },
];
