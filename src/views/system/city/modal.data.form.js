// 新增/编辑开通运营的城市弹窗表单配置
export const addOrEditFormConfig = [
          {
            prop: 'city',
            type: 'input',
            label: '城市',
            componentProp: {
              clearable: true,
              placeholder: '请输入城市',
            },
            componentEvent: {},
            rules: [{ required: true, trigger: 'blur', message: '城市不能为空' }],
          },
          {
            prop: 'cityGeocode',
            type: 'input',
            label: '城市编码',
            componentProp: {
              clearable: true,
              placeholder: '请输入城市编码',
            },
            componentEvent: {},
            rules: [{ required: true, trigger: 'blur', message: '城市编码不能为空' }],
          },
          {
            prop: 'lon',
            type: 'input',
            label: '经度',
            componentProp: {
              clearable: true,
              placeholder: '请输入经度',
            },
            componentEvent: {},
            rules: [{ required: true, trigger: 'blur', message: '经度不能为空' }],
          },
          {
            prop: 'lat',
            type: 'input',
            label: '纬度',
            componentProp: {
              clearable: true,
              placeholder: '请输入纬度',
            },
            componentEvent: {},
            rules: [{ required: true, trigger: 'blur', message: '纬度不能为空' }],
          },
          {
            prop: 'sort',
            type: 'input',
            label: '排序',
            componentProp: {
              clearable: true,
              placeholder: '请输入排序',
            },
            componentEvent: {},
            rules: [{ required: true, trigger: 'blur', message: '排序不能为空' }],
          },
          {
            prop: 'cc',
            type: 'input',
            label: 'cc',
            componentProp: {
              clearable: true,
              placeholder: '请输入cc',
            },
            componentEvent: {},
            rules: [{ required: true, trigger: 'blur', message: 'cc不能为空' }],
          },
];
