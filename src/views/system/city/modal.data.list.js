// 页面搜索条件配置
export const searchColumns = (that) => [
        {
          prop: 'city',
          type: 'input',
          label: '城市',
          componentProp: {
            clearable: true,
            placeholder: '请输入城市',
          },
          componentEvent: {},
        },
        {
          prop: 'cityGeocode',
          type: 'input',
          label: '城市编码',
          componentProp: {
            clearable: true,
            placeholder: '请输入城市编码',
          },
          componentEvent: {},
        },
        {
          prop: 'lon',
          type: 'input',
          label: '经度',
          componentProp: {
            clearable: true,
            placeholder: '请输入经度',
          },
          componentEvent: {},
        },
        {
          prop: 'lat',
          type: 'input',
          label: '纬度',
          componentProp: {
            clearable: true,
            placeholder: '请输入纬度',
          },
          componentEvent: {},
        },
        {
          prop: 'sort',
          type: 'input',
          label: '排序',
          componentProp: {
            clearable: true,
            placeholder: '请输入排序',
          },
          componentEvent: {},
        },
        {
          prop: 'cc',
          type: 'input',
          label: 'cc',
          componentProp: {
            clearable: true,
            placeholder: '请输入cc',
          },
          componentEvent: {},
        },
];

// 页面表格配置
export const tableSchema = [
      {
        type: 'selection',
        fixed: 'left',
      },
      {
        label: '城市',
        dataIndex: 'city',
        width: '180',
      },
      {
        label: '城市编码',
        dataIndex: 'cityGeocode',
        width: '180',
      },
      {
        label: '经度',
        dataIndex: 'lon',
        width: '180',
      },
      {
        label: '纬度',
        dataIndex: 'lat',
        width: '180',
      },
      {
        label: '排序',
        dataIndex: 'sort',
        width: '180',
      },
      {
        label: 'cc',
        dataIndex: 'cc',
        width: '180',
      },
];

// 表格操作配置
export const tableActions = (handleUpdate, handleStatusChange, handleShowDetail) => [
  {
    width: 220,
    label: '操作',
    buttons: [
      { label: '查看详情', onClick: (row) => { handleShowDetail(row); } },
      { label: '编辑', onClick: (row) => { handleUpdate(row); } },
      { label: '禁用', onClick: (row) => { handleStatusChange(row); } }
    ]
  }
];