/*
 * @Author: chenl
 * @Date: 2023-12-05 14:07:27
 * @LastEditors: chenl
 * @LastEditTime: 2024-03-11 17:16:04
 * @Description: 常用工具类
 */
import commonApis from '@/apis/common';
import { merchantList, factoryList, shopProductList } from './exportExcelJson';

var getUUID = function () {
  var S4 = function () {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  };
  return S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + S4() + S4();
};

export const elUpload = function (file, attachmentTypeCode) {
  return new Promise((resolve) => {
    let formDataByImage = new FormData();
    formDataByImage.append('file', file);
    formDataByImage.append('attachmentTypeCode', attachmentTypeCode);
    formDataByImage.append('fileName', file.name);
    commonApis.upload(formDataByImage).then((res) => {
      resolve(res.data);
    });
  });
};
