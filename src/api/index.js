import request from '@/utils/request';

// 城市工地地图相关数据获取
export function showCitySiteMap() {
  return request.get('/index/show/citySiteMap');
}

export function showDataOverview() {
  return request.get('/index/show/dataOverview');
}

export function showRealTimeException() {
  return request.get('/index/show/realTimeException');
}

export function showIndexPageBottom(params) {
  return request({
    url: '/index/show/indexPageBottom',
    method: 'get',
    params,
  });
}
