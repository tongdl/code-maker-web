import request from '@/utils/request';

export function addBusinesses(data) {
  return request.post('/businesses/add', data);
}

export function updateBusinesses(data) {
  return request.post('/businesses/update', data);
}

export function businessesPageList(data) {
  return request.get('/businesses/pageList', { params: data });
}

export function deleteBusinesses(id) {
  return request.get('/businesses/delete', { params: { id } });
}
