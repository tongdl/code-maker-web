import request from '@/utils/request';

export function addShip(data) {
  return request.post('/ship/add', data);
}

export function updateShip(data) {
  return request.post('/ship/edit', data);
}

export function updateShipCompany(idList, companyId) {
  return request.get('/ship/edit/company', { params: { idList, companyId } });
}

export function shipList(data) {
  return request.get('/ship/page', { params: data });
}

export function getShipDetail(id) {
  return request.get('/ship/detail', { params: { id } });
}
