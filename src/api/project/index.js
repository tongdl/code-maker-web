import request from '@/utils/request';

/**
 * @description 查询项目列表
 * @param data
 * @param params
 * @returns {*}
 */
export function pageList(data) {
  return request.post('/project/page', data);
}

export function addProject(data) {
  return request.post('/project/add', data);
}

export function updateProject(data) {
  return request.post('/project/edit', data);
}

export function getProjectDetail(id) {
  return request.get('/project/detail', { params: { id } });
}
