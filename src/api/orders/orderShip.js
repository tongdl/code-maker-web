import request from '@/utils/request';

/**
 * @description 查询列表
 * @param params
 * @returns {*}
 */
export function getList(data, params) {
  return request({
    url: '/orderShip/list',
    method: 'post',
    data,
    params,
  });
}

/**
 * @description 查询船舶订单
 * @param id
 * @returns {AxiosPromise}
 */
export function getOrderShipDetail(id) {
  return request.get('/orderShip/getDetail', { params: { id } });
}

/**
 * @description 发布船舶订单
 * @param data
 * @returns {*}
 */
export function addShipOrder(data) {
  return request({
    url: '/orderShip/add',
    method: 'post',
    data,
  });
}

/**
 * @description 编辑船舶订单
 * @param data
 * @returns {*}
 */
export function editShipOrder(data) {
  return request({
    url: '/orderShip/update',
    method: 'post',
    data,
  });
}

/**
 * @description 批量改变企业启用状态
 * @param params
 * @returns {*}
 */
export function batchUpdateStatus(params) {
  return request({
    url: '/orderShip/batchUpdateState',
    method: 'post',
    params,
  });
}

