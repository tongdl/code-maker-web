import request from '@/utils/request';

/**
 * @description 查询列表
 * @param params
 * @returns {*}
 */
export function getList(data, params) {
  return request({
    url: '/orderCargo/pageList',
    method: 'post',
    data,
    params,
  });
}

/**
 * @description 批量改变货物启用状态
 * @param params
 * @returns {*}
 */
export function batchUpdateStatus(params) {
  return request({
    url: '/orderCargo/batchUpdateState',
    method: 'get',
    params,
  });
}

/**
 * @description 发布货物订单
 * @param data
 * @returns {*}
 */
export function addCargoOrder(data) {
  return request({
    url: '/orderCargo/add',
    method: 'post',
    data,
  });
}

/**
 * @description 编辑货物订单
 * @param data
 * @returns {*}
 */
export function editCargoOrder(data) {
  return request({
    url: '/orderCargo/update',
    method: 'post',
    data,
  });
}

/**
 * @description 查询货物订单
 * @param id
 * @returns {AxiosPromise}
 */
export function getDetail(id) {
  return request({
    url: `/orderCargo/detail?id=${id}`,
    method: 'get',
  });
}
