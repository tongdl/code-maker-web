import request from '@/utils/request';

//获取项目订单列表
export function getOrderProjectList(params) {
  return request.get('/orderProject/pageList', { params });
}
//批量修改项目订单状态
export function setOrderState(params) {
  return request.get('/orderProject/batchUpdateState', { params });
}
//项目订单详情
export function getOrderProjectDetail(params) {
  return request.get('/orderProject/getDetail', { params });
}
//发布项目订单
export function addOrderProject(data) {
  return request.post('/orderProject/add', data);
}
//编辑项目订单
export function editOrderProject(data) {
  return request.post('/orderProject/update', data);
}
//获取归属企业
export function getCompanySelectList(params) {
  return request.get('/company/manage/getCompanyListByOperator', { params });
}
//获取归属企业下的项目
export function getProjectList(params) {
  return request.post('/project/getPageList', { params });
}
