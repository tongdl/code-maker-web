import request from '@/utils/request';

export function getPageList(data) {
  return request.get('/orderStaff/pageList', { params: data });
}

export function batchUpdateState(ids, state) {
  return request.get('/orderStaff/batchUpdateState', { params: { ids: ids, state: state } });
}

export function getDetail(id) {
  return request.get('/orderStaff/getDetail', { params: { id: id } });
}

export function add(id) {
  return request.get('/orderStaff/getDetail', { params: { id: id } });
}

export function update(id) {
  return request.get('/orderStaff/getDetail', { params: { id: id } });
}
