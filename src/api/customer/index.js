import request from '@/utils/request';

// 客户列表
export function operateManageList(params) {
  return request.get('/operate/manage/list', {
    params,
  });
}

// 启用禁用客户
export function updateStatus(deptId, status) {
  return request.post('/operate/manage/updateStatus', {
    deptId,
    status,
  });
}

// 新增客户
export function addOperateManage(data) {
  return request.post('/operate/manage', data);
}

// 修改客户
export function editOperateManage(data) {
  return request.put('/operate/manage', data);
}
