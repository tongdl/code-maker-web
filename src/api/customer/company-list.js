import request from '@/utils/request';

/**
 * @description 查询客户企业信息列表
 * @param params
 * @returns {*}
 */
export function companyList(data, params) {
  return request({
    url: '/jazx/company/getClientAExtraList',
    // method: 'get',
    method: 'post',
    data,
    params,
  });
}

/**
 * @description 查询客户企业信息详情
 * @param id
 * @returns {AxiosPromise}
 */
export function companyDetail(id) {
  return request({
    url: `/jazx/company/getClientBaseInfo/${id}`,
    method: 'get',
  });
}

/**
 * @description 新增客户企业信息
 * @param data
 * @returns {*}
 */
export function addCompany(data) {
  return request({
    url: '/jazx/company/insertClient',
    method: 'post',
    data,
  });
}

/**
 * @description 批量改变企业启用状态
 * @param params
 * @returns {*}
 */
export function batchUpdateStatus(params) {
  return request({
    url: '/jazx/company/zz/batchUpdateEnableStatus',
    method: 'post',
    params,
  });
}

/**
 * @description 操作员工离职
 * @param params
 * @returns {*}
 */
export function peopleLeave(params) {
  return request({
    url: '/jazx/company/clientUserDimissionNoFK',
    method: 'post',
    params,
  });
}

/**
 * @description 修改客户企业信息
 * @param data
 * @returns {*}
 */
export function updateCompany(data) {
  return request({
    url: '/jazx/company/editClientInfoAToFK',
    // method: 'put',
    method: 'post',
    data,
  });
}

/**
 * @description 同步企业信息
 * @param ids
 * @returns {AxiosPromise}
 */
export function syncCompanys(ids) {
  return request({
    url: `/jazx/company/syncUpdateCompany/${ids}`,
    method: 'get',
  });
}

/**
 * @description 客户管理_对选中的客户企业转移超管
 * @param data
 * @returns {*}
 */
export function transferSuperAdmin(data) {
  return request({
    url: '/jazx/company/transferSuperAdmin',
    method: 'put',
    params: data,
  });
}

/**
 * @description 查询当前选中的客户企业对应的所有月度学习计划，及其统计信息
 * @param companyId
 * @param otherQuery
 * @returns {AxiosPromise}
 */
export function monthStudyList(companyId, otherQuery) {
  return request({
    url: '/jazx/company/getClientMonthStudysStati',
    method: 'post',
    data: {
      ...otherQuery,
      companyId,
    },
  });
}

/**
 * @description XR: 查询当前选中的客户企业对应的所有XR课堂学习计划，及其统计信息
 * @param companyId
 * @param otherQuery
 * @returns {AxiosPromise}
 */
export function getClientXRPlansStati(companyId, otherQuery) {
  return request({
    url: '/jazx/company/getClientXRPlansStati',
    method: 'post',
    data: {
      ...otherQuery,
      companyId,
    },
  });
}
