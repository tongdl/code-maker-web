import request from '@/utils/request';

/**
 * 获取船舶类型下拉列表
 */
export function shipTypeEnum() {
  return request.get('/common/enum/shipType');
}

/**
 * 获取归属企业下拉列表
 */
export function getCompanyList() {
  return request.get('/company/manage/getCompanyListByOperator');
}

/**
 * 根据企业获取项目下拉列表
 */
export function getProjectListByCompany(companyId) {
  return request.get(`/project/list/by/companyId`, { params: { companyId } });
}

/**
 * 根据企业获取船舶下拉列表
 */
export function getShipListByCompany(companyId) {
  return request.get(`/ship/list/by/companyId`, { params: { companyId } });
}

/**
 * 获取归属运营商下拉列表
 */
export function getOperateList() {
  return request.get('/system/dept/getOperateAndChannel');
}

/**
 * 获取创建类型下拉列表
 */
export function createTypeEnum() {
  return request.get('/common/enum/createType');
}

/**
 * 获取平台状态下拉列表
 */
export function authStatusEnum() {
  return request.get('/common/enum/authStatus');
}

/**
 * 获取省市区树
 * @param level 1：省；2：省市；3：省市区
 */
export function getRegionTree(level) {
  return request.get('/common/getRegionTree', { params: { level } });
}

/**
 * 获取商户类型下拉列表
 */
export function getBusinessesList() {
  return request.get('/businesses/list');
}

/**
 * 获取证书等级下拉列表
 */
export function credentialsLevelEnum() {
  return request.get('/common/enum/credentialsLevel');
}

/**
 * 获取学历下拉列表
 */
export function educationEnum() {
  return request.get('/common/enum/education');
}

/**
 * 获取职务资格下拉列表
 */
export function credentialsTypeEnum() {
  return request.get('/common/enum/credentialsType');
}

/**
 * 获取性别下拉列表
 */
export function sexEnum() {
  return request.get('/common/enum/sex');
}

export function userTagEnum() {
  return request.get('/common/enum/userTag');
}

/**
 * 获取账号状态
 */
export function commonStatusEnum() {
  return request.get('/common/enum/commonStatus');
}

/**
 * 身份证ocr识别
 * @param fileUrl 身份证图片地址
 * @param side 华为云 front : 身份证人像；back : 身份证国徽
 */
export function idCardOcr(fileUrl, side) {
  return request.get('/common/idCardOcr', { params: { fileUrl, side } });
}

/**
 * @description 查询列表
 * @param params
 * @returns {*}
 */
export function getOperatorList(data, params) {
  return request({
    url: '/operate/manage/list',
    method: 'get',
    data,
    params,
  });
}
export function getPortList(data, params) {
  return request({
    url: '/common/getPortList',
    method: 'get',
    data,
    params,
  });
}
