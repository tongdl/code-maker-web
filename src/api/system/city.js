import request from '@/utils/request'

// 查询开通运营的城市列表
export function listCity(params) {
    return request.get('/system/city/list', params);
}

// 查询开通运营的城市详细
export function getCity(id) {
    return request.get('/system/city/' + id);
}

// 新增开通运营的城市
export function addCity(data) {
    return request.post('/system/city', data);
}

// 修改开通运营的城市
export function updateCity(data) {
    return request.put('/system/city', data);
}

// 删除开通运营的城市
export function delCity(id) {
    return request.delete('/system/city/' + id);
}
