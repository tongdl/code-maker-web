import request from '@/utils/request';

/**
 * @description 查询用户列表
 */
export function getPageList(data) {
  return request.post('/user/page', data);
}

/**
 * @description 查询用户列表
 * @param data
 * @returns {*}
 */
export function batchUpdateStatus(data) {
  return request.post('/user/edit/status/batch', data);
}

/**
 * @description 查询用户详情
 * @param id
 */
export function getUserDetail(id) {
  return request.get('/user/detail', { params: { id } });
}

export function getUserCompanyList(data) {
  return request.get('/company/user/list/by/userId', { params: { userId: data.userId } });
}

export function changeMobile(id, mobile) {
  return request.get('/user/edit/mobile', { params: { userId: id, mobile: mobile } });
}

export function joinCompany(userId, companyCodes) {
  return request.get('/user/join/company', {
    params: { userId: userId, companyCodes: companyCodes },
  });
}
