import request from '@/utils/request';
export function postCompanyManage(data) {
  return request.post('/company/manage', data);
}
// 编辑企业
export function putCompanyManage(data) {
  return request.put('/company/manage', data);
}
export function getManageList(params) {
  return request.get('/company/manage/list', { params });
}
export function getCompanyListByOperator(params) {
  return request.get('/company/manage/getCompanyListByOperator', { params });
}
export function getOperateAndChannel(params) {
  return request.get('/system/dept/getOperateAndChannel', { params });
}
// 103.批量禁用/恢复接口
export function updateStatus(id, status) {
  const data = {
    companyIds: id,
    status,
  };
  return request({
    url: '/company/manage/updateStatus',
    method: 'post',
    data: data,
  });
}

// 108.更新企业用户角色
export function updateCompanyUserRole(data) {
  return request.post(`/company/manage/user/role/edit`, data);
}

// 109.移出企业
export function leaveCompany(data) {
  return request.post(`/company/manage/leaveCompany`, data);
}

// 110.转移超管
export function transferLeader(data) {
  return request.post(`/company/manage/transferCompanySuperAdmin`, data);
}

export function getCompanyById(id) {
  return request.get(`/company/manage/query/${id}`);
}

export function fetchCompanyAuth(id) {
  return request.get(`/company/auth/${id}`);
}

export function updateCompanyAuth(data) {
  return request.post('/company/auth', data);
}

export function addCompanyAuth(data) {
  return request.post('/company/auth', data);
}

export function fetchCompanyWaterAuth(id) {
  return request.get(`/company/waterAuth/${id}`);
}

export function addCompanyWaterAuth(data) {
  return request.post('/company/waterAuth', data);
}

export function getAllCompany(data) {
  return request.post('/company/manage/list/all', data);
}
export function updateCompanyWaterAuth(data) {
  return request.post('/company/waterAuth', data);
}

export function getCompanyUserList(data) {
  return request.post('/company/user/page/by/companyId', data);
}

export function getCompanyUserListWithoutUserId(data) {
  return request.post('/company/user/list/by/companyId/without/userId', data);
}
