import request from '@/utils/request';

export function add(data) {
  return request.post('/crew/add', data);
}

export function update(data) {
  return request.post('/crew/edit', data);
}

export function getPageList(data) {
  return request.get('/crew/page', { params: data });
}

export function getDetail(id) {
  return request.get('/crew/detail', { params: { id } });
}

export function checkAuth(mobile) {
  return request.get('/crew/auth/check', { params: { mobile } });
}

/**
 * 获取船员下拉列表
 */
export function getCrewTreeList() {
  return request.get('/crew/list');
}
