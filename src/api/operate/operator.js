import request from '@/utils/request';

// 查询部门列表
export function listDept(query) {
  return request({
    url: '/operate/manage/list',
    method: 'get',
    params: query,
  });
}

// 查询运营商详细
export function getDept(deptId) {
  return request({
    url: '/operate/manage/getInfo/' + deptId,
    method: 'get',
  });
}

// 新增运营商
export function addDept(data) {
  return request({
    url: '/operate/manage',
    method: 'post',
    data: data,
  });
}

// 修改状态
export function updateStatus(data) {
  return request({
    url: '/operate/manage/updateStatus',
    method: 'post',
    data: data,
  });
}

// 修改部门
export function updateDept(data) {
  return request({
    url: '/operate/manage',
    method: 'put',
    data: data,
  });
}
