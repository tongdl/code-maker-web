import request from '@/utils/request';

export function addWharf(data) {
  return request.post('/wharf/add', data);
}

export function deleteWharf(id) {
  return request.get('/wharf/delete', { params: { id } });
}

export function updateWharf(data) {
  return request.post('/wharf/update', data);
}

export function getWharfPageList(data) {
  return request.get('/wharf/pageList', { params: data });
}

export function getWharfDetail(id) {
  return request.get('/wharf/getDetail', { params: { id } });
}

export function batchUpdateStatus(ids, status) {
  return request.get('/wharf/batchUpdateStatus', { params: { ids: ids, status: status } });
}

export function batchUpdateBusinesses(ids, businessesId) {
  return request.get('/wharf/batchUpdateBusinesses', {
    params: { ids: ids, businessesId: businessesId },
  });
}
