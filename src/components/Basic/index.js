import BasicSearchForm from '@/components/Basic/SearchForm/index.vue';
import BasicForm from '@/components/Basic/Form/index.vue';
import BasicTable from '@/components/Basic/Table';

export const BasicComponents = {
  install(Vue) {
    Vue.component('BasicSearchForm', BasicSearchForm);
    Vue.component('BasicTable', BasicTable);
    Vue.component('BasicForm', BasicForm);
  },
};
