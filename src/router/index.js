/*
 * @Author: chenl
 * @Date: 2023-12-11 11:27:34
 * @LastEditors: chenl
 * @LastEditTime: 2023-12-22 17:01:31
 * @Description:
 */
import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/* Layout */
import Layout from '@/layout';

/**
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * roles: ['admin', 'common']       // 访问路由的角色权限
 * permissions: ['a:a:a', 'b:b:b']  // 访问路由的菜单权限
 * meta : {
 noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
 title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
 icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
 breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
 activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
 }
 */

// 公共路由
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect'),
      },
    ],
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true,
  },
  {
    path: '/register',
    component: () => import('@/views/login/register'),
    hidden: true,
  },
  {
    path: '/reset-password',
    component: () => import('@/views/login/reset-password'),
    hidden: true,
  },
  {
    path: '/404',
    component: () => import('@/views/error/404'),
    hidden: true,
  },
  {
    path: '/401',
    component: () => import('@/views/error/401'),
    hidden: true,
  },
  {
    path: '',
    component: Layout,
    redirect: 'index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/index'),
        name: 'Index',
        meta: { title: '首页', icon: 'dashboard', affix: true },
      },
    ],
  },
  {
    path: '/user',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'profile',
        component: () => import('@/views/system/user/profile/index'),
        name: 'Profile',
        meta: { title: '个人中心', icon: 'user' },
      },
    ],
  },
];

// 动态路由，基于用户权限动态去加载
export const dynamicRoutes = [
  {
    path: '/system/user-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:user:edit'],
    children: [
      {
        path: 'role/:userId(\\d+)',
        component: () => import('@/views/system/user/authRole'),
        name: 'AuthRole',
        meta: { title: '分配角色', activeMenu: '/system/user' },
      },
    ],
  },
  {
    path: '/system/role-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:role:edit'],
    children: [
      {
        path: 'user/:roleId(\\d+)',
        component: () => import('@/views/system/role/authUser'),
        name: 'AuthUser',
        meta: { title: '分配用户', activeMenu: '/system/role' },
      },
    ],
  },
  {
    path: '/system/dict-data',
    component: Layout,
    hidden: true,
    permissions: ['system:dict:list'],
    children: [
      {
        path: 'index/:dictId(\\d+)',
        component: () => import('@/views/system/dict/data'),
        name: 'Data',
        meta: { title: '字典数据', activeMenu: '/system/dict' },
      },
    ],
  },
  {
    path: '/monitor/job-log',
    component: Layout,
    hidden: true,
    permissions: ['monitor:job:list'],
    children: [
      {
        path: 'index/:jobId(\\d+)',
        component: () => import('@/views/monitor/job/log'),
        name: 'JobLog',
        meta: { title: '调度日志', activeMenu: '/monitor/job' },
      },
    ],
  },
  {
    path: '/tool/gen-edit',
    component: Layout,
    hidden: true,
    permissions: ['tool:gen:edit'],
    children: [
      {
        path: 'index/:tableId(\\d+)',
        component: () => import('@/views/tool/gen/editTable'),
        name: 'GenEdit',
        meta: { title: '修改生成配置', activeMenu: '/tool/gen' },
      },
    ],
  },
  // ====================gcb用户=========================
  {
    path: '/user/list',
    component: Layout,
    hidden: true,
    permissions: ['gcb:user:detail'],
    children: [
      {
        path: 'detail',
        component: () => import('@/views/user/detail/index.vue'),
        name: 'UserDetail',
        meta: { title: '用户详情', activeMenu: '/user/list/detail' },
      },
    ],
  },
  // ====================船舶=========================
  {
    path: '/ship/list/save',
    component: Layout,
    hidden: true,
    permissions: ['gcb:ship:save'],
    children: [
      {
        path: 'save',
        component: () => import('@/views/platform/ship/save/index.vue'),
        name: 'SaveShip',
        meta: { title: '保存船舶', activeMenu: '/ship/list' },
      },
    ],
  },
  {
    path: '/ship/list/detail',
    component: Layout,
    hidden: true,
    permissions: ['gcb:ship:getDetail'],
    children: [
      {
        path: 'detail',
        component: () => import('@/views/platform/ship/detail/index.vue'),
        name: 'ShipDetail',
        meta: { title: '船舶详情', activeMenu: '/ship/list' },
      },
    ],
  },
  // ======================项目=========================
  {
    path: '/project/list',
    component: Layout,
    hidden: true,
    permissions: ['platform:project:save'],
    children: [
      {
        path: 'save',
        component: () => import('@/views/platform/project/list/save/index.vue'),
        name: 'SaveProject',
        meta: { title: '项目编辑', activeMenu: '/platform/project/list' },
      },
    ],
  },
  {
    path: '/project/list',
    component: Layout,
    hidden: true,
    permissions: ['platform:project:detail'],
    children: [
      {
        path: 'detail',
        component: () => import('@/views/platform/project/list/detail/index.vue'),
        name: 'ProjectDetail',
        meta: { title: '项目详情', activeMenu: '/platform/project/list' },
      },
    ],
  },
  // ======================船员=========================
  {
    path: '/crew/list',
    component: Layout,
    hidden: true,
    permissions: ['platform:crew:save'],
    children: [
      {
        path: 'save',
        component: () => import('@/views/platform/crew/save/index.vue'),
        name: 'CrewSave',
        meta: { title: '船员编辑', activeMenu: '/platform/crew/list' },
      },
    ],
  },
  {
    path: '/crew/list',
    component: Layout,
    hidden: true,
    permissions: ['platform:crew:detail'],
    children: [
      {
        path: 'detail',
        component: () => import('@/views/platform/crew/detail/index.vue'),
        name: 'CrewDetail',
        meta: { title: '船员详情', activeMenu: '/platform/crew/list' },
      },
    ],
  },
  // =======================订单=============================
  {
    path: '/orders/ship/',
    component: Layout,
    hidden: true,
    permissions: ['orders:ship:detail'],
    children: [
      {
        path: 'detail',
        component: () => import('@/views/orders/ship/detail/index.vue'),
        name: 'OrdersShipDetail',
        meta: { title: '船舶订单详情', activeMenu: '/orders/ship/list' },
      },
    ],
  },
  {
    path: '/orders/ship/',
    component: Layout,
    hidden: true,
    permissions: ['orders:ship:save'],
    children: [
      {
        path: 'save',
        component: () => import('@/views/orders/ship/save/index.vue'),
        name: 'OrdersShipSave',
        meta: { title: '船舶订单保存', activeMenu: '/orders/ship/list' },
      },
    ],
  },
  {
    path: '/orders/cargo/',
    component: Layout,
    hidden: true,
    permissions: ['orders:cargo:addoredit'],
    children: [
      {
        path: 'addcargo',
        component: () => import('@/views/orders/cargo/addOrUpdate.vue'),
        name: 'OrdersCargoAddOrEdit',
        meta: { title: '货源编辑', activeMenu: '/orders/cargo/list' },
      },
    ],
  },
  {
    path: '/orders/cargo/detail/',
    component: Layout,
    hidden: true,
    permissions: ['orders:cargo:detail'],
    children: [
      {
        path: 'detail',
        component: () => import('@/views/orders/cargo/detail.vue'),
        name: 'OrdersCargoDetail',
        meta: { title: '货源订单详情', activeMenu: '/orders/cargo/list' },
      },
    ],
  },
  {
    path: '/orders/project/',
    component: Layout,
    hidden: true,
    permissions: ['orders:project:save'],
    children: [
      {
        path: 'save',
        component: () => import('@/views/orders/project/save/index.vue'),
        name: 'SaveOrderProject',
        meta: { title: '项目订单保存', activeMenu: '/orders/project/list' },
      },
    ],
  },
  {
    path: '/orders/project/',
    component: Layout,
    hidden: true,
    permissions: ['orders:project:detail'],
    children: [
      {
        path: 'detail',
        component: () => import('@/views/orders/project/detail/index.vue'),
        name: 'OrderProjectDetail',
        meta: { title: '项目订单详情', activeMenu: '/orders/project/list' },
      },
    ],
  },
  {
    path: '/orders/staff',
    component: Layout,
    hidden: true,
    permissions: ['orders:staff:save'],
    children: [
      {
        path: 'save',
        component: () => import('@/views/orders/staff/save/index.vue'),
        name: 'OrderStaffSave',
        meta: { title: '求职订单保存', activeMenu: '/orders/staff/list' },
      },
    ],
  },
  {
    path: '/orders/staff',
    component: Layout,
    hidden: true,
    permissions: ['orders:staff:detail'],
    children: [
      {
        path: 'detail',
        component: () => import('@/views/orders/staff/detail/index.vue'),
        name: 'OrderStaffDetail',
        meta: { title: '求职订单详情', activeMenu: '/orders/staff/list' },
      },
    ],
  },
  // ======================码头=========================
  {
    path: '/wharf/list/save',
    component: Layout,
    hidden: true,
    permissions: ['businesses:wharf:save'],
    children: [
      {
        path: 'save',
        component: () => import('@/views/businesses/wharf/save/index.vue'),
        name: 'WharfSave',
        meta: { title: '保存码头', activeMenu: '/wharf/list' },
      },
    ],
  },
  {
    path: '/wharf/list/detail',
    component: Layout,
    hidden: true,
    permissions: ['businesses:wharf:getDetail'],
    children: [
      {
        path: 'detail',
        component: () => import('@/views/businesses/wharf/detail/index.vue'),
        name: 'WharfDetail',
        meta: { title: '码头详情', activeMenu: '/wharf/list' },
      },
    ],
  },
  {
    path: '/enterprise',
    component: Layout,
    hidden: true,
    permissions: ['enterprise:auth:form'],
    children: [
      {
        path: 'auth',
        component: () => import('@/views/enterprise/auth/index.vue'),
        name: 'CompanyAuth',
        meta: { title: '企业认证', activeMenu: '/enterprise/enterpriseList' },
      },
    ],
  },
  {
    path: '/enterprise',
    component: Layout,
    hidden: true,
    permissions: ['enterprise:waterAuth:form'],
    children: [
      {
        path: 'waterAuth',
        component: () => import('@/views/enterprise/waterAuth/index.vue'),
        name: 'CompanyWaterAuth',
        meta: { title: '企业船舶营运资质认证', activeMenu: '/enterprise/enterpriseList' },
      },
    ],
  },
  {
    path: '/enterprise',
    component: Layout,
    hidden: true,
    permissions: ['enterprise:detail'],
    children: [
      {
        path: 'auth/detail',
        component: () => import('@/views/enterprise/detail/index.vue'),
        name: 'CompanyDetail',
        meta: { title: '企业详情', activeMenu: '/enterprise/enterpriseList' },
      },
    ],
  },
];

// 防止连续点击多次路由报错
let routerPush = Router.prototype.push;
let routerReplace = Router.prototype.replace;
// push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch((err) => err);
};
// replace
Router.prototype.replace = function push(location) {
  return routerReplace.call(this, location).catch((err) => err);
};

export default new Router({
  mode: 'history', // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes,
});
