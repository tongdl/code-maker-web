/*
 * @Author: chenl
 * @Date: 2023-12-05 16:24:58
 * @LastEditors: chenl
 * @LastEditTime: 2023-12-05 16:25:09
 * @Description: 总线层
 */
const Thread = {
  data() {
    return {
      loading: false,
    };
  },
  mounted() {
    this.handBus();
  },
  methods: {
    // 查询
    handBus() {
      this.$eventHub.on('showloading', () => {
        this.loading = true;
      });
      this.$eventHub.on('closeLoading', () => {
        this.loading = false;
      });
    },
  },
};

export default Thread;
