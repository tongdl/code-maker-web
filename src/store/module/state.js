/*
 * @Author: chenl
 * @Date: 2023-12-04 18:04:40
 * @LastEditors: chenl
 * @LastEditTime: 2024-03-13 19:27:01
 * @Description: 状态管理
 */

// import commonApis from "@/apis/common.js";

const state = {
  // 示例
  shili: '示例',
  // 菜单显示隐藏
  isCollapse: false,
  // 获取当前菜单位置
  menuPosition: '首页',
  // 获取所点击的路由信息
  menuTabs: [
    {
      route: 'stoneManage',
      name: '砂石料场管理',
    },
  ],
};

// 类似于computed(计算属性，对现有的状态进行计算得到新的数据-------派生 )
const getters = {
  // 示例
  shili: (state) => state.shili,
  isCollapse: (state) => state.isCollapse,
  menuPosition: (state) => state.menuPosition,
  menuTabs: (state) => state.menuTabs,
};

// 发起异步请求
const actions = {
  updateisCollapseAsync({ commit }, value) {
    commit('updateisCollapse', value);
  },
  updateMenuPositionAsync({ commit }, value) {
    commit('updateMenuPosition', value);
  },
  updateMenuTabsAsync({ commit }, value) {
    commit('updateMenuTabs', value);
  },
};

// 使用它来修改数据(类似于methods)更改state中的数据必须使用mutations来进行更改
const mutations = {
  updateShili(state, value) {
    console.log(state, value);
  },
  updateisCollapse(state, value) {
    state.isCollapse = value;
  },
  updateMenuPosition(state, value) {
    state.menuPosition = value;
  },
  updateMenuTabs(state, value) {
    state.menuTabs = value;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
