import Vue from 'vue';
import Vuex from 'vuex';
import app from './modules/app';
import dict from './modules/dict';
import user from './modules/user';
import tagsView from './modules/tagsView';
import permission from './modules/permission';
import settings from './modules/settings';
import getters from './getters';
// import createPersistedstate from "vuex-persistedstate";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    app,
    dict,
    user,
    tagsView,
    permission,
    settings,
  },
  getters,
  // plugins: [
  //   createPersistedstate({
  //     key: "dlb-jazx-web", // 存数据的key名   自定义的  要有语义化
  //     storage: window.sessionStorage
  //     // paths: ['loginModule'] // 要把那些模块加入缓存,没加全局缓存
  //   }),
  // ],
});

export default store;
