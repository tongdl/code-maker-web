## 开发

```bash
# 克隆项目
git clone https://git.xmhold.com:3449/admintlb/dlb-jazx-web.git

# 进入项目目录
cd dlb-jazx-web

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npmmirror.com

## 设置全局路径（可选）
npm config set prefix "E:\Program Files\nodejs\node_global"

## 设置缓存路径（可选）
npm config set cache "E:\Program Files\nodejs\node_cache"


# 安装依赖(这里强烈推荐使用pnpm)
npm install -g pnpm

# 安装yarn
npm install --global yarn
# 设置镜像源
yarn config set registry https://registry.npmmirror.com
# 查看当前镜像源
yarn config get registry


# 启动服务 (开发环境)、需要运行后端
npm run dev
OR
yarn dev
OR
pnpm dev

# 启动服务 (测试环境)、使用的是测试服务
npm run test
OR
yarn test
OR
pnpm test

```

浏览器访问 http://localhost:80

## 提交代码

```bash

# 提交变更
git add .

# 规范化提交文本、代码格式检测、自动格式修复工具
git-czg
```

## 发布

```bash
# 构建测试环境
npm run build:test
OR
yarn build:test

# 构建生产环境
npm run build:prod
OR
yarn build:prod
```

## 地址

正式环境:
jazx.dalubao.cn

测试环境:
jazxtest.dalubao.cn
