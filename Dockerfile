# 设置 nginx 作为静态资源服务器
# 指定基础镜像nginx:latest
FROM nginx:latest

# 将我们自定义的网站静态文件复制到容器中
COPY dist/ /home/gcb-test-ui/
# 将我们自定义的nginx配置文件复制到容器中
COPY nginx.conf /etc/nginx/conf.d/default.conf

# 暴露82端口
EXPOSE 82

# 启动 nginx 服务器
CMD ["nginx", "-g", "daemon off;"]
