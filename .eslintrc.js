module.exports = {
  env: {
    node: true,
    browser: true,
    es2021: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:vue/essential',
    'plugin:vue/strongly-recommended',
    'plugin:prettier/recommended',
  ],
  overrides: [
    {
      env: {
        node: true,
      },
      files: ['.eslintrc.{js,cjs}'],
      parserOptions: {
        sourceType: 'script',
      },
    },
  ],
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ['vue'],
  rules: {
    'no-unused-vars': 0,
    'vue/multi-word-component-names': 0,
    'no-require-imports': 0,
    'vue/no-side-effects-in-computed-properties': 0,
    'vue/require-default-prop': 0,
    'no-debugger': 0,
    'vue/require-prop-types': 0,
    'vue/no-mutating-props': 0,
    'vue/no-reserved-component-names': 0,
  },
};
